import * as fs from 'fs-extra';
import * as glob from 'glob';
import * as path from 'path';
import * as mkdir from 'make-dir';

const EXECUTION_PATH = process.cwd();
const INPUT_DIRNAME = 'input';
const OUTPUT_DIRNAME = 'output';
const SEARCH_PATTERN = [
  '**/*.css',
  '**/*.js',
  '**/*.html',
  '**/*.json',
];

const REPLACE_RGXP = /betcity\.net/gi;
const REPLACE_WIDTH = 'betcity.ni';

class Program {
  private get inputDir(): string {
    return path.join(EXECUTION_PATH, INPUT_DIRNAME);
  }

  private get outputDir(): string {
    return path.join(EXECUTION_PATH, OUTPUT_DIRNAME);
  }

  main(): void {
    this.initFolders();
    const files = this.getInputFileList();

    for (const fileName of files) {
      console.log(`Replacing file ${fileName}...`);

      const content = this.readFile(fileName);

      const newFileName = this.replaceInputFileName(fileName);
      const newContent = this.replaceFileContent(content);

      this.writeFile(newFileName, newContent);
    }

    console.log('DONE.');
  }

  private replaceInputFileName(inputFileName: string): string {
    const inputFileNameCut = inputFileName.split(this.inputDir)[1];
    const outputFileName = path.join(this.outputDir, inputFileNameCut);

    return outputFileName;
  }

  private replaceFileContent(content: string): string {
    return content.replace(REPLACE_RGXP, REPLACE_WIDTH);
  }

  private readFile(filename: string): string {
    return fs.readFileSync(filename, { encoding: 'utf-8' });
  }

  private writeFile(filename: string, content: string): void {
    fs.mkdirpSync(path.dirname(filename));

    return fs.writeFileSync(filename, content, { encoding: 'utf-8' });
  }

  private initFolders(): void {
    mkdir.sync(this.inputDir);
    mkdir.sync(this.outputDir);

    fs.emptyDirSync(this.outputDir);
    this.copyInputToOutput();
  }

  private copyInputToOutput(): void {
    fs.copySync(this.inputDir, this.outputDir, { overwrite: true, recursive: true });
  }
  
  private getInputFileList(): Array<string> {
    const result: Array<string> = [];

    for (const pattern of SEARCH_PATTERN) {
      result.push(...glob.sync(pattern, { cwd: this.inputDir, absolute: true }));
    }

    return result;
  }
}

const program = new Program();
program.main();
